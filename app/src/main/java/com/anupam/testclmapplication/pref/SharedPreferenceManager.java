package com.anupam.testclmapplication.pref;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by anupamdutta on 05/03/17.
 */

public class SharedPreferenceManager {

    private Context mContext;
    private SharedPreferences mPref;

    public static final String PREF_NAME = "Anupam_Pref";
    public static final String KEY_FIRST_LOAD = "firstLoad";

    public SharedPreferenceManager(Context context) {
        this.mContext = context;
        mPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public boolean isFirstLoad() {
        return mPref.getBoolean(KEY_FIRST_LOAD, true);
    }

    public void setFirstLoad(boolean firstLoadStatus) {
        mPref.edit().putBoolean(KEY_FIRST_LOAD, firstLoadStatus).commit();
    }

}
