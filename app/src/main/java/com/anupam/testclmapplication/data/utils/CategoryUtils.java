package com.anupam.testclmapplication.data.utils;

import com.anupam.testclmapplication.data.category.Category;
import com.anupam.testclmapplication.data.category.CategoryItem;
import com.anupam.testclmapplication.util.api.google.response.GoogleSearchApiResponse;
import com.anupam.testclmapplication.util.api.google.response.GoogleSearchItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anupamdutta on 05/03/17.
 */

public class CategoryUtils {

    private static final String[] CATEGORY_NAME_LIST = {
            "Cars",
            "Bikes",
            "Mobiles",
            "Electronics",
            "Fashion",
            "Furniture"
    };

    public static List<Category> getCategories(GoogleSearchApiResponse response) {
        List<Category> categoryList = null;
        if (response != null && response.getGoogleSearchItems() != null && response.getGoogleSearchItems().size() > 0) {
            categoryList = new ArrayList<>();
            List<GoogleSearchItem> items = response.getGoogleSearchItems();
            for (GoogleSearchItem item : items) {

                if (item != null && item.getTitle() != null && item.getGoogleSearchImage() != null
                        && item.getGoogleSearchImage().getThumbnailLink() != null) {

                    Category category = new Category();
                    category.title = item.getTitle();
                    category.imageUrl = item.getLink();
                    if (item.getGoogleSearchImage() != null) {
                        category.smallImageUrl = item.getGoogleSearchImage().getThumbnailLink();
                    }
                    categoryList.add(category);
                }
            }
        }

        return categoryList;
    }

    public static List<CategoryItem> getCategoryItems(GoogleSearchApiResponse response) {
        List<CategoryItem> categoryItemList = null;
        if (response != null && response.getGoogleSearchItems() != null && response.getGoogleSearchItems().size() > 0) {
            categoryItemList = new ArrayList<>();
            List<GoogleSearchItem> items = response.getGoogleSearchItems();
            for (GoogleSearchItem item : items) {

                if (item != null && item.getTitle() != null && item.getGoogleSearchImage() != null
                        && item.getGoogleSearchImage().getThumbnailLink() != null) {

                    CategoryItem categoryItem = new CategoryItem();
                    categoryItem.title = item.getTitle();
                    categoryItem.imageUrl = item.getLink();
                    if (item.getGoogleSearchImage() != null) {
                        categoryItem.smallImageUrl = item.getGoogleSearchImage().getThumbnailLink();
                    }
                    categoryItemList.add(categoryItem);
                }
            }
        }

        return categoryItemList;
    }

    public static String[] getFixedCategoryNames() {
        return CATEGORY_NAME_LIST;
    }

    public static int getFixedCategoryCount() {
        return CATEGORY_NAME_LIST.length;
    }


    public static List<Category> getFixedCategories(List<Category> categoryList) {

        if (categoryList == null || categoryList.size() == 0) {
            return null;
        }

        final int fixedCategorySize = getFixedCategoryCount();
        List<Category> newList = new ArrayList<>(fixedCategorySize);

        int index = 0;

        for (Category category : categoryList) {
            if (category != null && category.imageUrl != null && category.smallImageUrl != null) {
                category.title = CATEGORY_NAME_LIST[index++];
                newList.add(category);

                if (index >= fixedCategorySize) {
                    break;
                }
            }
        }

        return newList;
    }


}
