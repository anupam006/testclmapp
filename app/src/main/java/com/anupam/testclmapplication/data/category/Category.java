package com.anupam.testclmapplication.data.category;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by anupamdutta on 05/03/17.
 */

public class Category implements Parcelable {

    public String title;
    public String imageUrl;
    public String smallImageUrl;

    @Override
    public String toString() {
        return "Title = " + (title != null ? title : "") + " Link = " + (imageUrl != null ? imageUrl : "") + " HashCode = " + super.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.imageUrl);
        dest.writeString(this.smallImageUrl);
    }

    public Category() {
    }

    protected Category(Parcel in) {
        this.title = in.readString();
        this.imageUrl = in.readString();
        this.smallImageUrl = in.readString();
    }

    public static final Parcelable.Creator<Category> CREATOR = new Parcelable.Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel source) {
            return new Category(source);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
}
