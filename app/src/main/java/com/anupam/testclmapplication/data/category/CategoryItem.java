package com.anupam.testclmapplication.data.category;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by anupamdutta on 05/03/17.
 */

public class CategoryItem implements Parcelable {

    public String title;
    public String imageUrl;
    public String smallImageUrl;

    @Override
    public String toString() {
        return "Title = " + (title != null ? title : "") + " Link = " + (imageUrl != null ? imageUrl : "") + " HashCode = " + super.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.imageUrl);
        dest.writeString(this.smallImageUrl);
    }

    public CategoryItem() {
    }

    protected CategoryItem(Parcel in) {
        this.title = in.readString();
        this.imageUrl = in.readString();
        this.smallImageUrl = in.readString();
    }

    public static final Parcelable.Creator<CategoryItem> CREATOR = new Parcelable.Creator<CategoryItem>() {
        @Override
        public CategoryItem createFromParcel(Parcel source) {
            return new CategoryItem(source);
        }

        @Override
        public CategoryItem[] newArray(int size) {
            return new CategoryItem[size];
        }
    };
}
