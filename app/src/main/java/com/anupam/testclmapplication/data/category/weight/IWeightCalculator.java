package com.anupam.testclmapplication.data.category.weight;

import com.anupam.testclmapplication.data.category.Category;

import java.util.List;

/**
 * Created by anupamdutta on 05/03/17.
 */

public interface IWeightCalculator {

    public int getWeight(Category category);

    public int getWeight(Category category, List<Category> list);

}
