package com.anupam.testclmapplication.tracking;

/**
 * Created by anupamdutta on 06/03/17.
 */

public enum UserEvent {

    CATEGORY_OPEN,
    CATEGORY_ITEM_OPEN

}
