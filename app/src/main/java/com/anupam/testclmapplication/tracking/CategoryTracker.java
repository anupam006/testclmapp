package com.anupam.testclmapplication.tracking;

import android.content.Context;

import com.anupam.clmtracking.tracking.data.TrackingData;
import com.anupam.clmtracking.tracking.factory.TrackerFactoryUtil;
import com.anupam.clmtracking.tracking.tracker.Tracker;

/**
 * Created by anupamdutta on 06/03/17.
 */

public class CategoryTracker {

    private Context mContext;
    private Tracker mTracker;


    public CategoryTracker(Context context) {
        mContext = context;
        mTracker = TrackerFactoryUtil.getDefaultTrackerFactory().getTracker();
        mTracker.init(context);
    }

    public void trackEvent(UserEvent event, String eventValue) {
        switch (event) {
            case CATEGORY_OPEN: {
                TrackingData data = new TrackingData();
                data.trackingEventType = event.toString();
                data.trackingEventValue = eventValue;
                mTracker.track(data);
                break;
            }
            case CATEGORY_ITEM_OPEN: {
                TrackingData data = new TrackingData();
                data.trackingEventType = event.toString();
                data.trackingEventValue = eventValue;
                mTracker.track(data);
                break;
            }
            default:

        }
    }

}
