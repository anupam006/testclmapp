package com.anupam.testclmapplication.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.anupam.testclmapplication.R;
import com.anupam.testclmapplication.data.category.CategoryItem;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by anupamdutta on 05/03/17.
 */

public class CategoryItemListAdapter extends RecyclerView.Adapter<CategoryItemListAdapter.CategoryItemViewHolder> {

    private Context mContext;
    private List<CategoryItem> mCategoryItemList;

    public static interface ItemClickListener {
        void onItemClicked(CategoryItem data, View view);
    }

    private CategoryItemListAdapter.ItemClickListener mItemClickListener;


    public CategoryItemListAdapter(Context context, List<CategoryItem> list, ItemClickListener listener) {
        mContext = context;
        mCategoryItemList = list;
        mItemClickListener = listener;
    }

    @Override
    public CategoryItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_item_list_item, parent, false);
        return new CategoryItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoryItemViewHolder holder, final int position) {

        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemClickListener.onItemClicked(mCategoryItemList.get(position), v);
            }
        });

        final CategoryItem item = mCategoryItemList.get(position);

        if (!TextUtils.isEmpty(item.smallImageUrl)) {
            Picasso.with(mContext).load(mCategoryItemList.get(position).smallImageUrl).placeholder(R.mipmap.ic_launcher).into(holder.ivIcon);
        } else {
            Picasso.with(mContext).load(mCategoryItemList.get(position).imageUrl).placeholder(R.mipmap.ic_launcher).into(holder.ivIcon);
        }

    }

    @Override
    public int getItemCount() {
        return mCategoryItemList.size();
    }

    public static class CategoryItemViewHolder extends RecyclerView.ViewHolder {

        public View rootView;
        public TextView tvTitle;
        public ImageView ivIcon;

        public CategoryItemViewHolder(View view) {
            super(view);
            rootView = view;
            tvTitle = (TextView) view.findViewById(R.id.title);
            ivIcon = (ImageView) view.findViewById(R.id.icon);
        }
    }

}
