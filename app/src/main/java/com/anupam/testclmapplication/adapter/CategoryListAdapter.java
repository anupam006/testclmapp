package com.anupam.testclmapplication.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.anupam.testclmapplication.R;
import com.anupam.testclmapplication.data.category.Category;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by anupamdutta on 05/03/17.
 */

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.CategoryViewHolder> {

    public static interface ItemClickListener {
        void onItemClicked(Category data, View view);
    }

    private ItemClickListener mItemClickListener;

    private Context mContext;
    private List<Category> mCategoryList;

    public CategoryListAdapter(Context context, List<Category> list, ItemClickListener listener) {
        mContext = context;
        mCategoryList = list;
        mItemClickListener = listener;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_list_item, parent, false);
        return new CategoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, final int position) {

        Category item = mCategoryList.get(position);

        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemClickListener.onItemClicked(mCategoryList.get(position), v);
            }
        });

        holder.tvTitle.setText(mCategoryList.get(position).title);

        if (!TextUtils.isEmpty(item.smallImageUrl)) {
            Picasso.with(mContext).load(item.smallImageUrl).placeholder(R.mipmap.ic_launcher).into(holder.ivIcon);
        } else {
            Picasso.with(mContext).load(item.imageUrl).placeholder(R.mipmap.ic_launcher).into(holder.ivIcon);
        }

    }

    @Override
    public int getItemCount() {
        return mCategoryList.size();
    }

    public static class CategoryViewHolder extends RecyclerView.ViewHolder {

        public View rootView;
        public TextView tvTitle;
        public ImageView ivIcon;

        public CategoryViewHolder(View view) {
            super(view);
            rootView = view;
            tvTitle = (TextView) view.findViewById(R.id.title);
            ivIcon = (ImageView) view.findViewById(R.id.icon);
        }
    }

}
