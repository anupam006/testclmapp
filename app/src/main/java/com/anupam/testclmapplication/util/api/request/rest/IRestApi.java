package com.anupam.testclmapplication.util.api.request.rest;

/**
 * Created by anupamdutta on 05/03/17.
 */

public interface IRestApi {

    public String requestGet(String url);

}
