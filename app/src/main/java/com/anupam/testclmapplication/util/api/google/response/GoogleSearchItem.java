package com.anupam.testclmapplication.util.api.google.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GoogleSearchItem {

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("link")
    @Expose
    private String link;

    @SerializedName("snippet")
    @Expose
    private String snippet;

    @SerializedName("mime")
    @Expose
    private String mime;

    @SerializedName("image")
    @Expose
    private GoogleSearchImage googleSearchImage;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSnippet() {
        return snippet;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public GoogleSearchImage getGoogleSearchImage() {
        return googleSearchImage;
    }

    public void setGoogleSearchImage(GoogleSearchImage googleSearchImage) {
        this.googleSearchImage = googleSearchImage;
    }

}
