package com.anupam.testclmapplication.util.api.google.response.parser;

import com.anupam.testclmapplication.util.api.google.response.GoogleSearchApiResponse;

/**
 * Created by anupamdutta on 05/03/17.
 */

public interface IGoogleResponseParser {

    public GoogleSearchApiResponse getResponse(String response);

}
