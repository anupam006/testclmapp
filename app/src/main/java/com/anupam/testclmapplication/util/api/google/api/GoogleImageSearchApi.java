package com.anupam.testclmapplication.util.api.google.api;

import com.anupam.testclmapplication.util.api.google.config.GoogleSearchApiKey;
import com.anupam.testclmapplication.util.api.google.request.GoogleSearchApiRequest;
import com.anupam.testclmapplication.util.api.google.response.GoogleSearchApiResponse;
import com.anupam.testclmapplication.util.api.google.response.parser.IGoogleResponseParser;
import com.anupam.testclmapplication.util.api.request.rest.IRestApi;

/**
 * Created by anupamdutta on 05/03/17.
 */

public class GoogleImageSearchApi implements IGoogleSearchApi {

    private IRestApi mRestApi;
    private GoogleSearchApiKey mKey;
    private IGoogleResponseParser mJsonResponseParser;

    public GoogleImageSearchApi(IRestApi restApi, IGoogleResponseParser parser) {
        this.mRestApi = restApi;
        this.mJsonResponseParser = parser;
    }

    @Override
    public GoogleSearchApiKey getKey() {
        return mKey;
    }

    @Override
    public void setKey(GoogleSearchApiKey key) {
        this.mKey = key;
    }

    @Override
    public GoogleSearchApiResponse query(GoogleSearchApiRequest request) {
        GoogleSearchApiResponse response = new GoogleSearchApiResponse();
        String jsonResponse = mRestApi.requestGet(request.getQuery());
        response = mJsonResponseParser.getResponse(jsonResponse);
        return response;
    }
}
