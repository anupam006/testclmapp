package com.anupam.testclmapplication.util.api.google.factory;


import com.anupam.testclmapplication.util.api.google.api.IGoogleSearchApi;

/**
 * Created by anupamdutta on 05/03/17.
 */

public interface IGoogleSearchApiFactory {
    public IGoogleSearchApi getInstance();
}
