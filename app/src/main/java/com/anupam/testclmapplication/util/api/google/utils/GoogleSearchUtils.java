package com.anupam.testclmapplication.util.api.google.utils;

import com.anupam.testclmapplication.util.api.google.api.IGoogleSearchApi;
import com.anupam.testclmapplication.util.api.google.config.DefaultGoogleSearchApiKey;
import com.anupam.testclmapplication.util.api.google.config.GoogleSearchApiKey;
import com.anupam.testclmapplication.util.api.google.factory.GoogleImageSearchApiFactory;
import com.anupam.testclmapplication.util.api.google.factory.IGoogleSearchApiFactory;
import com.anupam.testclmapplication.util.api.google.request.GoogleSearchApiRequest;
import com.anupam.testclmapplication.util.api.google.response.GoogleSearchApiResponse;

/**
 * Created by anupamdutta on 05/03/17.
 */

public abstract class GoogleSearchUtils {

    public static IGoogleSearchApiFactory getDefaultGoogleSearchApiFactory() {
        return new GoogleImageSearchApiFactory();
    }

    public static IGoogleSearchApi getDefaultGoogleSearchApi() {
        return getDefaultGoogleSearchApiFactory().getInstance();
    }

    public static GoogleSearchApiKey getDefaultGoogleSearchApiKey() {
        return new DefaultGoogleSearchApiKey();
    }

    public static GoogleSearchApiRequest getGooleSearchApiRequest(String search) {
        return new GoogleSearchApiRequest(search, getDefaultGoogleSearchApiKey());
    }

    public static GoogleSearchApiResponse query(String search) {
        GoogleSearchApiRequest request = getGooleSearchApiRequest(search);
        IGoogleSearchApi api = getDefaultGoogleSearchApi();
        return api.query(request);
    }

}
