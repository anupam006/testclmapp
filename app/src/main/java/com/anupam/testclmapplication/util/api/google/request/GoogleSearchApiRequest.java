package com.anupam.testclmapplication.util.api.google.request;


import com.anupam.testclmapplication.util.api.google.config.GoogleSearchApiKey;

/**
 * Created by anupamdutta on 05/03/17.
 */

public class GoogleSearchApiRequest {

    private static String baseUrl = "https://www.googleapis.com/customsearch/v1?searchType=image";
    private String searchTerm;
    private GoogleSearchApiKey key;

    public GoogleSearchApiRequest(String searchTerm, GoogleSearchApiKey key) {
        this.searchTerm = searchTerm;
        this.key = key;
    }

    public String getQuery() {
        StringBuilder sb = new StringBuilder(baseUrl);

        // Add Key
        sb.append("&");
        sb.append(getKeyParam(key.getKey()));

        // Add Search Engine
        sb.append("&");
        sb.append(getSearchEngineParam(key.getCx()));

        // Add Search Term
        sb.append("&");
        sb.append(getSearchParam(searchTerm));

        return sb.toString();
    }

    private String getSearchParam(String searchTerm) {
        return "q=" + searchTerm;
    }

    private String getKeyParam(String key) {
        return "key=" + key;
    }

    private String getSearchEngineParam(String searchEngine) {
        return "cx=" + searchEngine;
    }

}
