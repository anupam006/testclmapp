package com.anupam.testclmapplication.util.api.google.config;

/**
 * Created by anupamdutta on 05/03/17.
 * <p>
 */

public abstract class GoogleSearchApiKey {

    /**
     * Google Search Engine Id
     */
    private String cx;
    /**
     * Google Search Engine Key
     */
    private String key;

    public String getCx() {
        return cx;
    }

    public void setCx(String cx) {
        this.cx = cx;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public GoogleSearchApiKey(String cx, String key) {
        this.cx = cx;
        this.key = key;
    }
}
