package com.anupam.testclmapplication.util.api.google.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GoogleSearchApiResponse {

    @SerializedName("items")
    @Expose
    private List<GoogleSearchItem> googleSearchItems = null;

    public List<GoogleSearchItem> getGoogleSearchItems() {
        return googleSearchItems;
    }

    public void setGoogleSearchItems(List<GoogleSearchItem> googleSearchItems) {
        this.googleSearchItems = googleSearchItems;
    }

}
