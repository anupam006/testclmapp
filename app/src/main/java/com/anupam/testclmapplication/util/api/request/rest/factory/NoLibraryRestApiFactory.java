package com.anupam.testclmapplication.util.api.request.rest.factory;

import com.anupam.testclmapplication.util.api.request.rest.IRestApi;
import com.anupam.testclmapplication.util.api.request.rest.httpclient.HttpClientRestApi;

/**
 * Created by anupamdutta on 05/03/17.
 */

public class NoLibraryRestApiFactory implements IRestApiFactory {

    @Override
    public IRestApi getRestApi() {
        return new HttpClientRestApi();
    }

}
