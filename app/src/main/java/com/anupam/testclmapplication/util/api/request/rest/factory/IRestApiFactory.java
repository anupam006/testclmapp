package com.anupam.testclmapplication.util.api.request.rest.factory;

import com.anupam.testclmapplication.util.api.request.rest.IRestApi;

/**
 * Created by anupamdutta on 05/03/17.
 */

public interface IRestApiFactory {

    public IRestApi getRestApi();

}
