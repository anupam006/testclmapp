package com.anupam.testclmapplication.util.api.google.factory;

import com.anupam.testclmapplication.util.api.google.api.GoogleImageSearchApi;
import com.anupam.testclmapplication.util.api.google.api.IGoogleSearchApi;
import com.anupam.testclmapplication.util.api.google.response.parser.GsonGoogleResponseParser;
import com.anupam.testclmapplication.util.api.request.rest.factory.NoLibraryRestApiFactory;

/**
 * Created by anupamdutta on 05/03/17.
 */

public class GoogleImageSearchApiFactory implements IGoogleSearchApiFactory {


    @Override
    public IGoogleSearchApi getInstance() {
        return new GoogleImageSearchApi(new NoLibraryRestApiFactory().getRestApi(), new GsonGoogleResponseParser());
    }
}
