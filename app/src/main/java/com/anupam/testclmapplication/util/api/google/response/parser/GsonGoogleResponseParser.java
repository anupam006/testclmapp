package com.anupam.testclmapplication.util.api.google.response.parser;

import com.anupam.testclmapplication.util.api.google.response.GoogleSearchApiResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by anupamdutta on 05/03/17.
 */

public class GsonGoogleResponseParser implements IGoogleResponseParser {

    private Gson gson = new GsonBuilder().create();

    @Override
    public GoogleSearchApiResponse getResponse(String response) {
        GoogleSearchApiResponse res = gson.fromJson(response, GoogleSearchApiResponse.class);
        return res;
    }
}
