package com.anupam.testclmapplication.util.api.google.config;

/**
 * Created by anupamdutta on 05/03/17.
 * <p>
 */

public class DefaultGoogleSearchApiKey extends GoogleSearchApiKey {

    // TODO: We should obfuscate this
    public static String DEFAULT_KEY = "AIzaSyA1gl2xOb71b-p5zsCV-4doFCUf1lTIOfc";
    public static String DEFAULT_CX = "011882465376919089076:suvdburbyky";

    public DefaultGoogleSearchApiKey() {
        super(DEFAULT_CX, DEFAULT_KEY);
    }
}
