package com.anupam.testclmapplication.util.api.google.api;

import com.anupam.testclmapplication.util.api.google.config.GoogleSearchApiKey;
import com.anupam.testclmapplication.util.api.google.request.GoogleSearchApiRequest;
import com.anupam.testclmapplication.util.api.google.response.GoogleSearchApiResponse;

/**
 * Created by anupamdutta on 05/03/17.
 */

public interface IGoogleSearchApi {

    public GoogleSearchApiKey getKey();

    public void setKey(GoogleSearchApiKey key);

    public GoogleSearchApiResponse query(GoogleSearchApiRequest request);

}
