package com.anupam.testclmapplication.util.api.request.rest.httpclient;

import com.anupam.testclmapplication.util.api.request.rest.IRestApi;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by anupamdutta on 05/03/17.
 */

public class HttpClientRestApi implements IRestApi {

    @Override
    public String requestGet(String urlString) {

        URL url = null;
        HttpURLConnection urlConnection = null;
        String response = null;

        try {
            url = new URL(urlString);
            urlConnection = (HttpURLConnection) url.openConnection();

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            response = readStream(in);

        } catch (IOException e) {
            e.printStackTrace();
            response = null;
        } finally {
            urlConnection.disconnect();
        }

        return response;
    }

    private String readStream(InputStream inputStream) throws IOException {
        String res = null;
        final int bufferSize = 1024;
        final char[] buffer = new char[bufferSize];
        final StringBuilder out = new StringBuilder();
        Reader in = new InputStreamReader(inputStream, "UTF-8");
        while (true) {
            int rsz = in.read(buffer, 0, buffer.length);
            if (rsz < 0) {
                break;
            }
            out.append(buffer, 0, rsz);
        }
        return out.toString();
    }

}
