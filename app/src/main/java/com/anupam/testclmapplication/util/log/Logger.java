package com.anupam.testclmapplication.util.log;

import android.util.Log;

import com.anupam.testclmapplication.BuildConfig;

/**
 * Created by anupamdutta on 05/03/17.
 */

public class Logger {

    public static final String DEFAULT_TAG = "ANUPAM";

    public static void log(String tag, String message) {
        if (BuildConfig.DEBUG_LOG) {
            log(BuildConfig.LOG_LEVEL, tag, message);
        }
    }

    public static void log(String message) {
        log(DEFAULT_TAG, message);
    }

    private static void log(int logLevel, String tag, String message) {
        switch (logLevel) {
            case Log.VERBOSE: {
                Log.v(tag, message);
                break;
            }
            case Log.DEBUG: {
                Log.d(tag, message);
                break;
            }
            case Log.INFO: {
                Log.i(tag, message);
                break;
            }
            case Log.WARN: {
                Log.w(tag, message);
                break;
            }
            case Log.ERROR: {
                Log.e(tag, message);
                break;
            }
            case Log.ASSERT: {
                Log.e(tag, message);
                break;
            }
        }
    }

}
