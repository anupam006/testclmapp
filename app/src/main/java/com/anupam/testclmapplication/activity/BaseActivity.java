package com.anupam.testclmapplication.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by anupamdutta on 05/03/17.
 */


public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
