package com.anupam.testclmapplication.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;

import com.anupam.testclmapplication.R;
import com.anupam.testclmapplication.fragments.list.categoryitem.CategoryItemListEqualWeightedFragment;

/**
 * Created by anupamdutta on 05/03/17.
 */
public class CategoryItemListActivity extends BaseActivity {

    private static final String KEY_CATEGORY = "KEY_CATEGORY";

    public static Intent getIntent(Context context, String category) {
        Intent intent = new Intent(context, CategoryItemListActivity.class);
        intent.putExtra(KEY_CATEGORY, category);
        return intent;
    }

    public static Bundle getBundleWithCategory(String category) {
        Bundle bundle = new Bundle();
        bundle.putString(KEY_CATEGORY, category);
        return bundle;
    }

    public static String getCategoryFromBundle(Bundle bundle) {
        return bundle.getString(KEY_CATEGORY);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_item_list);
        initFragment(getIntent().getExtras());

    }


    public void initFragment(Bundle bundle) {

        Fragment fragment = null;
        String tag = null;
        Class fragmentClass = null;

        if (bundle == null || TextUtils.isEmpty(getCategoryFromBundle(bundle))) {
            finish();
            return;
        }

        int fragmentContainerId = R.id.content_frame;

        fragmentClass = CategoryItemListEqualWeightedFragment.class;
        tag = CategoryItemListEqualWeightedFragment.TAG;

        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragment = fragmentManager.findFragmentByTag(tag);
            if (fragment == null) {
                fragment = (Fragment) fragmentClass.newInstance();

                if (bundle != null) {
                    fragment.setArguments(bundle);
                }

                fragmentTransaction.replace(fragmentContainerId, fragment, tag);
                fragmentTransaction.commit();
            }

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }

}
