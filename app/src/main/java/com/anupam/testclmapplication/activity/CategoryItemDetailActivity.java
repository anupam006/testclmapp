package com.anupam.testclmapplication.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.anupam.testclmapplication.R;
import com.anupam.testclmapplication.data.category.CategoryItem;
import com.anupam.testclmapplication.fragments.detail.item.CategoryItemDetailFragment;

/**
 * Created by anupamdutta on 05/03/17.
 */
public class CategoryItemDetailActivity extends BaseActivity {

    private static final String KEY_CATEGORY = "KEY_CATEGORY";

    public static Intent getIntent(Context context, CategoryItem categoryItem) {
        Intent intent = new Intent(context, CategoryItemDetailActivity.class);
        intent.putExtra(KEY_CATEGORY, categoryItem);
        return intent;
    }

    public static Bundle getBundleWithCategoryItem(CategoryItem categoryItem) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_CATEGORY, categoryItem);
        return bundle;
    }

    public static CategoryItem getCategoryItemFromBundle(Bundle bundle) {
        return bundle.getParcelable(KEY_CATEGORY);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_detail);
        initFragment(getIntent().getExtras());

    }


    public void initFragment(Bundle bundle) {

        Fragment fragment = null;
        String tag = null;
        Class fragmentClass = null;

        if (bundle == null || getCategoryItemFromBundle(bundle) == null) {
            finish();
            return;
        }

        int fragmentContainerId = R.id.content_frame;

        fragmentClass = CategoryItemDetailFragment.class;
        tag = CategoryItemDetailFragment.TAG;

        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragment = fragmentManager.findFragmentByTag(tag);
            if (fragment == null) {
                fragment = (Fragment) fragmentClass.newInstance();

                if (bundle != null) {
                    fragment.setArguments(bundle);
                }

                fragmentTransaction.replace(fragmentContainerId, fragment, tag);
                fragmentTransaction.commit();
            }

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }

}
