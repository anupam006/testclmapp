package com.anupam.testclmapplication.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.anupam.clmtracking.tracking.tracker.offline.helper.TrackingTable;
import com.anupam.clmtracking.tracking.tracker.query.api.TrackingDataWithCountQueryApi;
import com.anupam.clmtracking.tracking.tracker.query.data.TrackingDataWithCount;
import com.anupam.clmtracking.tracking.tracker.query.factory.SqliteTrackingDataWithCountQueryApiFactory;
import com.anupam.testclmapplication.R;
import com.anupam.testclmapplication.fragments.home.NonFirstTimeHomeFragment;
import com.anupam.testclmapplication.fragments.list.category.CategoryListEqualWeightedFragment;
import com.anupam.testclmapplication.fragments.list.category.CategoryListWeightedFragment;
import com.anupam.testclmapplication.pref.SharedPreferenceManager;

import java.util.List;

/**
 * Created by anupamdutta on 05/03/17.
 */
public class HomeActivity extends BaseActivity {


    private SharedPreferenceManager mPref;
    private ViewType mCurrentViewType;
    private TrackingDataWithCountQueryApi mTrackingDataQueryApi;
    protected List<TrackingDataWithCount> mTrackingDataWithCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        init();


    }

    private void init() {
        mTrackingDataQueryApi = new SqliteTrackingDataWithCountQueryApiFactory().getTrackingDataWithCountQueryApi();
        mTrackingDataQueryApi.init(this);
        mTrackingDataWithCount = mTrackingDataQueryApi
                .getTrackingDataWithCount(
                        TrackingTable.Column.COLUMN_NAME_EVENT_VALUE, true);
        mPref = new SharedPreferenceManager(this);
        setView(getInitialViewType(), false);
    }

    public void setView(ViewType viewType, boolean addToBackStack) {

        Fragment fragment = null;
        String tag = null;
        Class fragmentClass = null;
        Bundle bundle = null;
        int fragmentContainerId = R.id.content_frame;

        switch (viewType) {
            case FIRST_TIME_VIEW: {
                fragmentClass = CategoryListWeightedFragment.class;
                tag = CategoryListWeightedFragment.TAG;
                break;
            }
            case SELECT_CATEGORY_WEIGHTED: {
                fragmentClass = CategoryListWeightedFragment.class;
                tag = CategoryListWeightedFragment.TAG;
                break;
            }
            case NON_FIRST_TIME: {
                fragmentClass = NonFirstTimeHomeFragment.class;
                tag = NonFirstTimeHomeFragment.TAG;
                break;
            }
        }

        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragment = fragmentManager.findFragmentByTag(tag);
            if (fragment == null) {
                fragment = (Fragment) fragmentClass.newInstance();

                if (bundle != null) {
                    fragment.setArguments(bundle);
                }

                if (addToBackStack) {
                    fragmentTransaction.addToBackStack(tag);
                }

                fragmentTransaction.replace(fragmentContainerId, fragment, tag);
                fragmentTransaction.commit();
            }

            mCurrentViewType = viewType;

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    private enum ViewType {
        FIRST_TIME_VIEW,
        NON_FIRST_TIME,
        SELECT_CATEGORY_WEIGHTED,
    }

    private ViewType getInitialViewType() {

        ViewType viewType = null;

        // Check if its first load or Not
        if (mTrackingDataWithCount != null && mTrackingDataWithCount.size() > 0) {
            viewType = ViewType.NON_FIRST_TIME;
//            Toast.makeText(this, "First Load", Toast.LENGTH_SHORT).show();
        } else {
            viewType = ViewType.FIRST_TIME_VIEW;
//            Toast.makeText(this, "2nd Time Load", Toast.LENGTH_SHORT).show();
        }

        return viewType;
    }

    public void openWeightedCategoryPage() {
        setView(ViewType.SELECT_CATEGORY_WEIGHTED, true);
    }

}
