package com.anupam.testclmapplication.fragments.list.category;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anupam.clmtracking.tracking.tracker.offline.helper.TrackingTable;
import com.anupam.clmtracking.tracking.tracker.query.api.TrackingDataWithCountQueryApi;
import com.anupam.clmtracking.tracking.tracker.query.data.TrackingDataWithCount;
import com.anupam.clmtracking.tracking.tracker.query.factory.SqliteTrackingDataWithCountQueryApiFactory;
import com.anupam.testclmapplication.R;
import com.anupam.testclmapplication.activity.CategoryItemListActivity;
import com.anupam.testclmapplication.adapter.CategoryListAdapter;
import com.anupam.testclmapplication.data.category.Category;
import com.anupam.testclmapplication.data.utils.CategoryUtils;
import com.anupam.testclmapplication.fragments.BaseFragment;
import com.anupam.testclmapplication.loaders.categorylist.CategoryListLoader;
import com.anupam.testclmapplication.tracking.CategoryTracker;
import com.anupam.testclmapplication.tracking.UserEvent;

import java.util.List;
import java.util.Map;

/**
 * Created by anupamdutta on 05/03/17.
 */

public abstract class CategoryListFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<List<Category>>, ICategoryListWeighted {

    public static final int LOADER_CATEGORY_LIST = 100;

    private TextView mTvTitle;
    private RecyclerView mRvCategory;
    private CategoryListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.ItemAnimator mItemAnimator;
    private List<Category> mCategoryList;
    private CategoryTracker mCategoryTracker;
    private TrackingDataWithCountQueryApi mTrackingDataQueryApi;
    protected List<TrackingDataWithCount> mTrackingDataWithCount;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (isWeightedCategoryNeeded()) {
            mTrackingDataQueryApi = new SqliteTrackingDataWithCountQueryApiFactory().getTrackingDataWithCountQueryApi();
            mTrackingDataQueryApi.init(getActivity());
        }

        mCategoryTracker = new CategoryTracker(getActivity());
        mItemAnimator = new DefaultItemAnimator();
    }

    private RecyclerView.LayoutManager getLayoutManager() {
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), getItemMaxSpanSize());
        final int CATEGORY_COLUMN_COUNT = getItemMaxSpanSize();

        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return getItemSpanSize(position);
            }
        });

        return layoutManager;
    }

    private void initWeights() {
        mRvCategory.setLayoutManager(getLayoutManager());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category_list, container, false);
        mTvTitle = (TextView) view.findViewById(R.id.tvTitle);
        mTvTitle.setText(getString(R.string.interested));
        mRvCategory = (RecyclerView) view.findViewById(R.id.rvList);
        mRvCategory.setLayoutManager(mLayoutManager);
        mRvCategory.setItemAnimator(mItemAnimator);
        return view;
    }


    @Override
    public Loader<List<Category>> onCreateLoader(int id, Bundle args) {
        Loader<List<Category>> loader = null;
        switch (id) {
            case LOADER_CATEGORY_LIST: {
                loader = new CategoryListLoader(getActivity(), "Cats");
            }
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<List<Category>> loader, List<Category> data) {

        mCategoryList = CategoryUtils.getFixedCategories(data);

        if (isWeightedCategoryNeeded()) {
            mTrackingDataWithCount = mTrackingDataQueryApi
                    .getTrackingDataWithCount(
                            TrackingTable.Column.COLUMN_NAME_EVENT_VALUE, true);

            initItemIndexSpanMap(mTrackingDataWithCount);
        }

        if (mCategoryList != null && mCategoryList.size() >= CategoryUtils.getFixedCategoryCount()) {
            initListWithData(mCategoryList);
        } else {
            mRvCategory.setAdapter(null);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Category>> loader) {

    }

    @Override
    public List<Category> getCategories() {
        return mCategoryList;
    }

    @Override
    public Category getCategory(int position) {
        if (mCategoryList != null && position <= mCategoryList.size() - 1) {
            return mCategoryList.get(position);
        } else {
            return null;
        }
    }

    private void initListWithData(List<Category> data) {

        mAdapter = new CategoryListAdapter(getActivity(), mCategoryList, mListener);
        initWeights();
        mRvCategory.setAdapter(mAdapter);
    }

    private final CategoryListAdapter.ItemClickListener mListener = new CategoryListAdapter.ItemClickListener() {
        @Override
        public void onItemClicked(Category data, View view) {
            mCategoryTracker.trackEvent(UserEvent.CATEGORY_OPEN, data.title);
            startActivity(CategoryItemListActivity.getIntent(getActivity(), data.title));
        }
    };

    @Override
    public int getItemSpanSize(int position) {

        Map<Integer, Integer> spanMap = getItemIndexSpanMap();

        if (spanMap == null) {
            return 1;
        }

        Integer integer = spanMap.get(position);

        if (integer != null) {
            return integer;
        } else {
            return 1;
        }

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().getSupportLoaderManager().initLoader(LOADER_CATEGORY_LIST, null, this);
    }
}
