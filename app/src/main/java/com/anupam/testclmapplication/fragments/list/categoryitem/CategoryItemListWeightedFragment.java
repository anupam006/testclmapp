package com.anupam.testclmapplication.fragments.list.categoryitem;

/**
 * Created by anupamdutta on 05/03/17.
 */

public class CategoryItemListWeightedFragment extends CategoryItemListFragment {

    public static final String TAG = CategoryItemListWeightedFragment.class.getSimpleName();

    public static final int COLUMN_SIZE = 3;

    @Override
    public int getItemMaxSpanSize() {
        return COLUMN_SIZE;
    }

    @Override
    public int getItemSpanSize(int position) {
        switch (position) {
            case 0:
                return getItemMaxSpanSize() - 1;
            case 5:
                return getItemMaxSpanSize();
            default:
                return 1;
        }
    }
}
