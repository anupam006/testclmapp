package com.anupam.testclmapplication.fragments.list.categoryitem;

/**
 * Created by anupamdutta on 05/03/17.
 */

public interface ICategoryItemListWeighted extends ICategoryItemList {

    public int getItemMaxSpanSize();

    public int getItemSpanSize(int position);


}
