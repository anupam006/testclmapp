package com.anupam.testclmapplication.fragments.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.anupam.clmtracking.tracking.tracker.offline.helper.TrackingTable;
import com.anupam.clmtracking.tracking.tracker.query.api.TrackingDataWithCountQueryApi;
import com.anupam.clmtracking.tracking.tracker.query.data.TrackingDataWithCount;
import com.anupam.clmtracking.tracking.tracker.query.factory.SqliteTrackingDataWithCountQueryApiFactory;
import com.anupam.testclmapplication.R;
import com.anupam.testclmapplication.activity.HomeActivity;
import com.anupam.testclmapplication.data.category.Category;
import com.anupam.testclmapplication.data.utils.CategoryUtils;
import com.anupam.testclmapplication.fragments.BaseFragment;
import com.anupam.testclmapplication.loaders.categorylist.CategoryListLoader;
import com.anupam.testclmapplication.tracking.CategoryTracker;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by anupamdutta on 05/03/17.
 */

public class NonFirstTimeHomeFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<List<Category>> {

    public static final String TAG = NonFirstTimeHomeFragment.class.getSimpleName();

    public static final String KEY_TRACKING_DATA = "Tracking Data";

    public static final int LOADER_CATEGORY_LIST = 100;

    private Category mCategory;
    private List<Category> mCategoryList;
    protected List<TrackingDataWithCount> mTrackingDataWithCount;
    private TrackingDataWithCountQueryApi mTrackingDataQueryApi;
    private CategoryTracker mCategoryTracker;


    private TextView mTitle;
    private TextView mTvCategory;
    private Button mBtnGotoCategory;
    private ImageView mIvIcon;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mTrackingDataQueryApi = new SqliteTrackingDataWithCountQueryApiFactory().getTrackingDataWithCountQueryApi();
        mTrackingDataQueryApi.init(getActivity());
        mTrackingDataWithCount = mTrackingDataQueryApi
                .getTrackingDataWithCount(
                        TrackingTable.Column.COLUMN_NAME_EVENT_VALUE, true);


        mCategoryTracker = new CategoryTracker(getActivity());

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_non_first_time, container, false);

        mTitle = (TextView) view.findViewById(R.id.tvTitle);
        mTvCategory = (TextView) view.findViewById(R.id.tvCategoryName);
        mBtnGotoCategory = (Button) view.findViewById(R.id.btnGotoCategory);
        mIvIcon = (ImageView) view.findViewById(R.id.ivIcon);

        mBtnGotoCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() instanceof HomeActivity) {
                    ((HomeActivity) getActivity()).openWeightedCategoryPage();
                }
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mTitle.setText(R.string.welcome);
        mBtnGotoCategory.setText(R.string.goto_category);
        getActivity().getSupportLoaderManager().initLoader(LOADER_CATEGORY_LIST, null, this);
    }

    @Override
    public Loader<List<Category>> onCreateLoader(int id, Bundle args) {
        Loader<List<Category>> loader = null;
        switch (id) {
            case LOADER_CATEGORY_LIST: {
                loader = new CategoryListLoader(getActivity(), "Cats");
            }
        }
        return loader;

    }

    @Override
    public void onLoadFinished(Loader<List<Category>> loader, List<Category> data) {

        mCategoryList = CategoryUtils.getFixedCategories(data);

        mTrackingDataWithCount = mTrackingDataQueryApi
                .getTrackingDataWithCount(
                        TrackingTable.Column.COLUMN_NAME_EVENT_VALUE, true);

        TrackingDataWithCount dataWithCount = null;
        if (mTrackingDataWithCount != null && mTrackingDataWithCount.size() > 0) {
            dataWithCount = mTrackingDataWithCount.get(0);
        }

        for (Category category : mCategoryList) {
            if (dataWithCount != null && dataWithCount.data != null && category != null && category.title.equals(dataWithCount.data)) {

                if (!TextUtils.isEmpty(category.title)) {
                    mTvCategory.setText(category.title);
                }

                if (!TextUtils.isEmpty(category.imageUrl)) {
                    Picasso.with(getActivity()).load(category.imageUrl)
                            .placeholder(R.mipmap.ic_launcher).into(mIvIcon);
                }

                break;
            }
        }

    }


    @Override
    public void onLoaderReset(Loader<List<Category>> loader) {

    }
}
