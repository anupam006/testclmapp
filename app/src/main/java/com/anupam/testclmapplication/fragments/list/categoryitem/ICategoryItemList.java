package com.anupam.testclmapplication.fragments.list.categoryitem;

import com.anupam.testclmapplication.data.category.CategoryItem;

import java.util.List;

/**
 * Created by anupamdutta on 05/03/17.
 */

public interface ICategoryItemList {

    public List<CategoryItem> getCategoryItems();

    public CategoryItem getCategoryItem(int position);

}
