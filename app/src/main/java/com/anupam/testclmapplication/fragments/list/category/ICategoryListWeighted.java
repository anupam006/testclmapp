package com.anupam.testclmapplication.fragments.list.category;

import com.anupam.clmtracking.tracking.tracker.query.data.TrackingDataWithCount;
import com.anupam.testclmapplication.data.category.Category;

import java.util.List;
import java.util.Map;

/**
 * Created by anupamdutta on 05/03/17.
 */

public interface ICategoryListWeighted {

    public List<Category> getCategories();

    public Category getCategory(int position);

    public int getItemMaxSpanSize();

    public int getItemSpanSize(int position);

    public Map<Integer, Integer> getItemIndexSpanMap();

    public boolean isWeightedCategoryNeeded();

    public void initItemIndexSpanMap(List<TrackingDataWithCount> trackingDataWithCountList);
}
