package com.anupam.testclmapplication.fragments.list.category;

import com.anupam.clmtracking.tracking.tracker.query.data.TrackingDataWithCount;
import com.anupam.testclmapplication.data.category.Category;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by anupamdutta on 05/03/17.
 */

public class CategoryListWeightedFragment extends CategoryListFragment {

    public static final String TAG = CategoryListWeightedFragment.class.getSimpleName();
    public static final int COLUMN_SIZE = 3;
    public static final int MAX_RESIZABLE = COLUMN_SIZE - 1;


    private Map<Integer, Integer> mSpanMap;

    @Override
    public int getItemMaxSpanSize() {
        return COLUMN_SIZE;
    }

    @Override
    public Map<Integer, Integer> getItemIndexSpanMap() {
        return mSpanMap;
    }

    @Override
    public boolean isWeightedCategoryNeeded() {
        return true;
    }

    @Override
    public void initItemIndexSpanMap(List<TrackingDataWithCount> trackingDataWithCountList) {

        if (trackingDataWithCountList == null || trackingDataWithCountList.size() == 0) {
            return;
        }

        int cnt = 0;
        mSpanMap = new HashMap<>();

        List<TrackingDataWithCount> topElements = new ArrayList<>();

        // Check the max value in Tracking Data List
        for (TrackingDataWithCount dataWithCount : trackingDataWithCountList) {
            topElements.add(dataWithCount);
            cnt++;
            if (cnt >= MAX_RESIZABLE) {
                break;
            }
        }

        int newReducedSize = topElements.size();
        int lastViewCount = 0;
        int level = 1;

        for (int i = newReducedSize - 1; i >= 0; i--) {
            int currentViewCount = topElements.get(i).count;
            if (lastViewCount < currentViewCount) {
                lastViewCount = currentViewCount;
                level++;
            }

            // Set Span Map Entry
            mSpanMap.put(findCategoryIndex(topElements.get(i).data), level);

        }
    }

    private int findCategoryIndex(String categoryName) {
        int count = 0;
        for (Category category : getCategories()) {
            if (category.title.equals(categoryName)) {
                break;
            } else {
                count++;
            }
        }
        return count;
    }

}
