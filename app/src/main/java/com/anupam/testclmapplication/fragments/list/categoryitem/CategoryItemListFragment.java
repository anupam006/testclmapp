package com.anupam.testclmapplication.fragments.list.categoryitem;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anupam.testclmapplication.R;
import com.anupam.testclmapplication.activity.CategoryItemDetailActivity;
import com.anupam.testclmapplication.activity.CategoryItemListActivity;
import com.anupam.testclmapplication.adapter.CategoryItemListAdapter;
import com.anupam.testclmapplication.data.category.CategoryItem;
import com.anupam.testclmapplication.data.utils.CategoryUtils;
import com.anupam.testclmapplication.fragments.BaseFragment;
import com.anupam.testclmapplication.loaders.categorylist.CategoryItemListLoader;

import java.util.List;

/**
 * Created by anupamdutta on 05/03/17.
 */

public abstract class CategoryItemListFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<List<CategoryItem>>, ICategoryItemListWeighted {

    public static final int LOADER_CATEGORY_ITEM_LIST = 100;

    private RecyclerView mRvList;
    private TextView mTvTitle;
    private CategoryItemListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.ItemAnimator mItemAnimator;
    private List<CategoryItem> mCategoryItemList;
    private String mCategory;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mItemAnimator = new DefaultItemAnimator();
        mCategory = CategoryItemListActivity.getCategoryFromBundle(getArguments());
        getActivity().getSupportLoaderManager().initLoader(LOADER_CATEGORY_ITEM_LIST, null, this);
    }

    private RecyclerView.LayoutManager getLayoutManager() {
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), getItemMaxSpanSize());

        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return getItemSpanSize(position);
            }
        });

        return layoutManager;
    }

    private void initWeights() {
        mRvList.setLayoutManager(getLayoutManager());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category_item_list, container, false);
        mTvTitle = (TextView) view.findViewById(R.id.tvTitle);
        mTvTitle.setText(mCategory + "  " + getString(R.string.results));
        mRvList = (RecyclerView) view.findViewById(R.id.rvList);
        mRvList.setLayoutManager(mLayoutManager);
        mRvList.setItemAnimator(mItemAnimator);
        return view;
    }


    @Override
    public Loader<List<CategoryItem>> onCreateLoader(int id, Bundle args) {
        Loader<List<CategoryItem>> loader = null;
        switch (id) {
            case LOADER_CATEGORY_ITEM_LIST: {
                loader = new CategoryItemListLoader(getActivity(), mCategory);
            }
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<List<CategoryItem>> loader, List<CategoryItem> data) {

        mCategoryItemList = data;

        if (mCategoryItemList != null && mCategoryItemList.size() >= CategoryUtils.getFixedCategoryCount()) {
            mAdapter = new CategoryItemListAdapter(getActivity(), mCategoryItemList, mListener);
            initWeights();
            mRvList.setAdapter(mAdapter);
        } else {
            mRvList.setAdapter(null);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<CategoryItem>> loader) {

    }

    @Override
    public List<CategoryItem> getCategoryItems() {
        return mCategoryItemList;
    }

    @Override
    public CategoryItem getCategoryItem(int position) {
        if (mCategoryItemList != null && position <= mCategoryItemList.size() - 1) {
            return mCategoryItemList.get(position);
        } else {
            return null;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    private final CategoryItemListAdapter.ItemClickListener mListener = new CategoryItemListAdapter.ItemClickListener() {
        @Override
        public void onItemClicked(CategoryItem data, View view) {
            startActivity(CategoryItemDetailActivity.getIntent(getActivity(), data));
        }

    };


}
