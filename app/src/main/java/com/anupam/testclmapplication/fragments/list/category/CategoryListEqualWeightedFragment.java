package com.anupam.testclmapplication.fragments.list.category;

import com.anupam.clmtracking.tracking.tracker.query.data.TrackingDataWithCount;

import java.util.List;
import java.util.Map;

/**
 * Created by anupamdutta on 05/03/17.
 */

public class CategoryListEqualWeightedFragment extends CategoryListFragment {

    public static final String TAG = CategoryListEqualWeightedFragment.class.getSimpleName();

    public static final int COLUMN_SIZE = 3;

    @Override
    public int getItemMaxSpanSize() {
        return COLUMN_SIZE;
    }

    @Override
    public int getItemSpanSize(int position) {
        return 1;
    }

    @Override
    public Map<Integer, Integer> getItemIndexSpanMap() {
        return null;
    }

    @Override
    public boolean isWeightedCategoryNeeded() {
        return false;
    }

    @Override
    public void initItemIndexSpanMap(List<TrackingDataWithCount> trackingDataWithCountList) {

    }

}
