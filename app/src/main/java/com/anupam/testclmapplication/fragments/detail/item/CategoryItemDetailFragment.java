package com.anupam.testclmapplication.fragments.detail.item;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.anupam.testclmapplication.R;
import com.anupam.testclmapplication.activity.CategoryItemDetailActivity;
import com.anupam.testclmapplication.data.category.CategoryItem;
import com.anupam.testclmapplication.fragments.BaseFragment;
import com.squareup.picasso.Picasso;

/**
 * Created by anupamdutta on 05/03/17.
 */

public class CategoryItemDetailFragment extends BaseFragment {

    public static final String TAG = CategoryItemDetailFragment.class.getSimpleName();

    private CategoryItem mCategoryItem;

    private TextView mTitle;
    private TextView mQuote;
    private ImageView mIvIcon;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCategoryItem = CategoryItemDetailActivity.getCategoryItemFromBundle(getArguments());

        if (mCategoryItem == null) {
            getActivity().finish();
            return;
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category_detail, container, false);
        mTitle = (TextView) view.findViewById(R.id.tvTitle);
        mQuote = (TextView) view.findViewById(R.id.tvQuote);
        mIvIcon = (ImageView) view.findViewById(R.id.ivIcon);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (mCategoryItem != null) {

            if (!TextUtils.isEmpty(mCategoryItem.title)) {
                mTitle.setText(mCategoryItem.title);
            }

            if (!TextUtils.isEmpty(mCategoryItem.imageUrl)) {
                Picasso.with(getActivity()).load(mCategoryItem.imageUrl)
                        .placeholder(R.mipmap.ic_launcher).into(mIvIcon);
            }

            mQuote.setText(R.string.quote);

        }

    }
}
