package com.anupam.testclmapplication.fragments.list.categoryitem;

/**
 * Created by anupamdutta on 05/03/17.
 */

public class CategoryItemListEqualWeightedFragment extends CategoryItemListFragment {

    public static final String TAG = CategoryItemListEqualWeightedFragment.class.getSimpleName();

    public static final int COLUMN_SIZE = 3;

    @Override
    public int getItemMaxSpanSize() {
        return COLUMN_SIZE;
    }

    @Override
    public int getItemSpanSize(int position) {
        return 1;
    }
}
