package com.anupam.testclmapplication.loaders.categorylist;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.text.TextUtils;

import com.anupam.testclmapplication.data.category.CategoryItem;
import com.anupam.testclmapplication.data.utils.CategoryUtils;
import com.anupam.testclmapplication.util.api.google.response.GoogleSearchApiResponse;
import com.anupam.testclmapplication.util.api.google.utils.GoogleSearchUtils;
import com.anupam.testclmapplication.util.log.Logger;

import java.util.List;

/**
 * Created by anupamdutta on 05/03/17.
 */

public class CategoryItemListLoader extends AsyncTaskLoader<List<CategoryItem>> {

    private static final String TAG = CategoryItemListLoader.class.getSimpleName();

    private String mQuery;

    public CategoryItemListLoader(Context context, String categoryName) {
        super(context);
        mQuery = categoryName;
    }

    @Override
    public List<CategoryItem> loadInBackground() {
        Logger.log(TAG, "Sending Request for = " + mQuery);

        if (TextUtils.isEmpty(mQuery)) {
            return null;
        }

        GoogleSearchApiResponse response = GoogleSearchUtils.query(mQuery);
        List<CategoryItem> categories = CategoryUtils.getCategoryItems(response);
        Logger.log(TAG, "Response = " + categories);
        return categories;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

}
