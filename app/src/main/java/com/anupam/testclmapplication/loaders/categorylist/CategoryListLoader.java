package com.anupam.testclmapplication.loaders.categorylist;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.anupam.testclmapplication.data.category.Category;
import com.anupam.testclmapplication.data.utils.CategoryUtils;
import com.anupam.testclmapplication.util.api.google.response.GoogleSearchApiResponse;
import com.anupam.testclmapplication.util.api.google.utils.GoogleSearchUtils;
import com.anupam.testclmapplication.util.log.Logger;

import java.util.List;

/**
 * Created by anupamdutta on 05/03/17.
 */

public class CategoryListLoader extends AsyncTaskLoader<List<Category>> {

    private static final String TAG = CategoryListLoader.class.getSimpleName();

    private String mQuery;

    public CategoryListLoader(Context context, String categoryName) {
        super(context);
        mQuery = categoryName;
    }

    @Override
    public List<Category> loadInBackground() {
        Logger.log(TAG, "Sending Request for = " + mQuery);
        GoogleSearchApiResponse response = GoogleSearchUtils.query(mQuery);
        List<Category> categories = CategoryUtils.getCategories(response);
        Logger.log(TAG, "Response = " + categories);
        return categories;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

}
