package com.anupam.clmtracking.tracking.tracker.offline.helper;

import android.database.sqlite.SQLiteDatabase;


/**
 * Created by anupamdutta on 06/03/17.
 */

public class SqliteOfflineDbUpgrader implements SqliteDbUpgrader {

    @Override
    public void upgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TrackingTable.TABLE_NAME);
    }
}
