package com.anupam.clmtracking.tracking.tracker.hybrid;

import android.content.Context;

import com.anupam.clmtracking.tracking.data.TrackingData;
import com.anupam.clmtracking.tracking.tracker.AbsTracker;
import com.anupam.clmtracking.tracking.tracker.Tracker;
import com.anupam.clmtracking.tracking.tracker.type.TrackerType;

import java.util.List;

/**
 * Created by anupamdutta on 06/03/17.
 */

public class AllInOneTracker extends AbsTracker {

    private List<Tracker> mTrackerlist;

    public AllInOneTracker(List<Tracker> trackerList) {
        super(TrackerType.ALL_IN_ONE);
        mTrackerlist = trackerList;
    }

    @Override
    public void track(TrackingData trackingData) {
        for (Tracker tracker : mTrackerlist) {
            tracker.track(trackingData);
        }
    }

    @Override
    public void init(Context context) {
        for (Tracker tracker : mTrackerlist) {
            tracker.init(context);
        }
    }
}
