package com.anupam.clmtracking.tracking.factory.type;


/**
 * Created by anupamdutta on 05/03/17.
 */

public enum TrackerFactoryType {
    OFFLINE,
    ONLINE,
    HYBRID,
}
