package com.anupam.clmtracking.tracking.tracker;


import com.anupam.clmtracking.tracking.tracker.type.TrackerType;

/**
 * Created by anupamdutta on 06/03/17.
 */

public abstract class AbsTracker implements Tracker {

    private TrackerType mTrackerType;

    public AbsTracker(TrackerType type) {
        mTrackerType = type;
    }

    @Override
    public TrackerType getTrackerType() {
        return mTrackerType;
    }

}
