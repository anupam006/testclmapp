package com.anupam.clmtracking.tracking.tracker.offline.helper;

import android.content.Context;

/**
 * Created by anupamdutta on 06/03/17.
 */

public class SqliteDbHelperUtil {

    public static SqliteOfflineTrackerDbHelper getDbHelper(Context context) {
        return new SqliteOfflineTrackerDbHelper(context, new SqliteOfflineDbCreator(),
                new SqliteOfflineDbUpgrader());
    }

}
