package com.anupam.clmtracking.tracking.tracker.online;

import android.content.Context;

import com.anupam.clmtracking.tracking.data.TrackingData;
import com.anupam.clmtracking.tracking.tracker.AbsTracker;
import com.anupam.clmtracking.tracking.tracker.type.TrackerType;

/**
 * Created by anupamdutta on 06/03/17.
 */

public class MixPanelTracker extends AbsTracker {

    public MixPanelTracker() {
        super(TrackerType.CLOUD_MIXPANEL);
    }

    @Override
    public void track(TrackingData trackingData) {

    }

    @Override
    public void init(Context context) {

    }
}
