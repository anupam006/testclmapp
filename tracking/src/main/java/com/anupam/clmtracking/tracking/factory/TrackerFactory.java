package com.anupam.clmtracking.tracking.factory;


import com.anupam.clmtracking.tracking.data.TrackingData;
import com.anupam.clmtracking.tracking.factory.type.TrackerFactoryType;
import com.anupam.clmtracking.tracking.tracker.Tracker;

/**
 * Created by anupamdutta on 05/03/17.
 */

public interface TrackerFactory {

    public Tracker getTracker();

    public TrackerFactoryType getTrackerFactoryType();

}
