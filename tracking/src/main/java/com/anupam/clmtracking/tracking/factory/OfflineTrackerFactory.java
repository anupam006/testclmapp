package com.anupam.clmtracking.tracking.factory;

import com.anupam.clmtracking.tracking.factory.type.TrackerFactoryType;
import com.anupam.clmtracking.tracking.tracker.offline.SqliteOfflineTracker;
import com.anupam.clmtracking.tracking.tracker.Tracker;

/**
 * Created by anupamdutta on 06/03/17.
 */

public class OfflineTrackerFactory extends AbsTrackerFactory {

    public OfflineTrackerFactory() {
        super(TrackerFactoryType.OFFLINE);
    }

    @Override
    public Tracker getTracker() {
        return new SqliteOfflineTracker();
    }
}
