package com.anupam.clmtracking.tracking.tracker.query.api;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.anupam.clmtracking.tracking.tracker.offline.helper.SqliteDbHelperUtil;
import com.anupam.clmtracking.tracking.tracker.offline.helper.SqliteOfflineTrackerDbHelper;
import com.anupam.clmtracking.tracking.tracker.offline.helper.TrackingTable;
import com.anupam.clmtracking.tracking.tracker.query.data.TrackingDataWithCount;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anupamdutta on 06/03/17.
 */

public class SqliteTrackingDataQueryApi implements TrackingDataWithCountQueryApi {

    private SqliteOfflineTrackerDbHelper mHelper;

    @Override
    public List<TrackingDataWithCount> getTrackingDataWithCount(String trackingDataColumn, boolean desc) {
        return getTrackingDataWithCount(trackingDataColumn, desc, null);
    }

    @Override
    public List<TrackingDataWithCount> getTrackingDataWithCount(String trackingDataColumn, boolean desc, String whereClause) {

        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ").append(trackingDataColumn).append(", count(1) cnt")
                .append(" FROM ").append(TrackingTable.TABLE_NAME)
                .append(" WHERE ").append(TextUtils.isEmpty(whereClause) ? " 1" : whereClause)
                .append(" GROUP BY ").append(trackingDataColumn)
                .append(" ORDER BY cnt ").append(desc ? " DESC" : " ASC").append(";");
        SQLiteDatabase mDb = mHelper.getReadableDatabase();
        Cursor cursor = mDb.rawQuery(sb.toString(), null);

        List<TrackingDataWithCount> list = new ArrayList<>();

        try {
            if (cursor != null) {
                cursor.moveToPosition(-1);
                while (cursor.moveToNext()) {
                    TrackingDataWithCount dataWithCount = new TrackingDataWithCount();
                    dataWithCount.data = cursor.getString(0);
                    dataWithCount.count = cursor.getInt(1);
                    list.add(dataWithCount);
                }
            }
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
                cursor = null;
            }
        }

        return list.size() > 0 ? list : null;
    }

    @Override
    public void init(Context context) {
        mHelper = SqliteDbHelperUtil.getDbHelper(context);
    }
}
