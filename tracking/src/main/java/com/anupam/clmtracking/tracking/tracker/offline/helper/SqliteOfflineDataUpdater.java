package com.anupam.clmtracking.tracking.tracker.offline.helper;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.anupam.clmtracking.tracking.data.TrackingData;

/**
 * Created by anupamdutta on 06/03/17.
 */

public class SqliteOfflineDataUpdater implements SqliteTrackingDataUpdater {

    @Override
    public void update(SQLiteDatabase database, TrackingData data) {
        ContentValues values = new ContentValues();
        values.put(TrackingTable.Column.COLUMN_NAME_EVENT_TYPE, data.trackingEventType);
        values.put(TrackingTable.Column.COLUMN_NAME_EVENT_VALUE, data.trackingEventValue);
        values.put(TrackingTable.Column.COLUMN_NAME_EVENT_TIMESTAMP, System.currentTimeMillis());
        database.insert(TrackingTable.TABLE_NAME, null, values);
    }
}
