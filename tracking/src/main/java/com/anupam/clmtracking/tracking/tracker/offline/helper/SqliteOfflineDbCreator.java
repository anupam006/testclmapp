package com.anupam.clmtracking.tracking.tracker.offline.helper;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by anupamdutta on 06/03/17.
 */

public class SqliteOfflineDbCreator implements SqliteDbCreator {

    private static final String DATABASE_CREATE = "create table "
            + TrackingTable.TABLE_NAME + "( "
            + TrackingTable.Column.COLUMN_NAME_ID + " integer primary key autoincrement, "
            + TrackingTable.Column.COLUMN_NAME_EVENT_TYPE + ", "
            + TrackingTable.Column.COLUMN_NAME_EVENT_VALUE + ", "
            + TrackingTable.Column.COLUMN_NAME_EVENT_TIMESTAMP + " text not null);";


    @Override
    public void createDb(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE);
    }

    private String getCreateDbQuery() {
        return DATABASE_CREATE;
    }

}
