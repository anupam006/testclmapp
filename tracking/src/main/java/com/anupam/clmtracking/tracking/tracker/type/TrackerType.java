package com.anupam.clmtracking.tracking.tracker.type;


/**
 * Created by anupamdutta on 05/03/17.
 */

public enum TrackerType {
    OFFLINE_SQLITE,
    CLOUD_MIXPANEL,
    ALL_IN_ONE,
}
