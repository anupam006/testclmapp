package com.anupam.clmtracking.tracking.tracker.query.factory;

import com.anupam.clmtracking.tracking.tracker.query.api.SqliteTrackingDataQueryApi;
import com.anupam.clmtracking.tracking.tracker.query.api.TrackingDataWithCountQueryApi;

/**
 * Created by anupamdutta on 06/03/17.
 */

public class SqliteTrackingDataWithCountQueryApiFactory implements TrackingDataWithCountQueryApiFactory {

    @Override
    public TrackingDataWithCountQueryApi getTrackingDataWithCountQueryApi() {
        return new SqliteTrackingDataQueryApi();
    }

}
