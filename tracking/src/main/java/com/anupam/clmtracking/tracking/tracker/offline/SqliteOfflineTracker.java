package com.anupam.clmtracking.tracking.tracker.offline;

import android.content.Context;

import com.anupam.clmtracking.tracking.data.TrackingData;
import com.anupam.clmtracking.tracking.tracker.AbsTracker;
import com.anupam.clmtracking.tracking.tracker.offline.helper.SqliteDbHelperUtil;
import com.anupam.clmtracking.tracking.tracker.offline.helper.SqliteOfflineDataUpdater;
import com.anupam.clmtracking.tracking.tracker.offline.helper.SqliteOfflineTrackerDbHelper;
import com.anupam.clmtracking.tracking.tracker.type.TrackerType;

/**
 * Created by anupamdutta on 06/03/17.
 */

public class SqliteOfflineTracker extends AbsTracker {

    private SqliteOfflineTrackerDbHelper mDbHelper;
    private SqliteOfflineDataUpdater mDbUpdater;

    public SqliteOfflineTracker() {
        super(TrackerType.OFFLINE_SQLITE);
    }

    @Override
    public void track(TrackingData trackingData) {
        mDbUpdater.update(mDbHelper.getWritableDatabase(), trackingData);
    }

    @Override
    public void init(Context context) {
        mDbHelper = SqliteDbHelperUtil.getDbHelper(context);
        mDbUpdater = new SqliteOfflineDataUpdater();
    }
}
