package com.anupam.clmtracking.tracking.factory;

import com.anupam.clmtracking.tracking.factory.type.TrackerFactoryType;

/**
 * Created by anupamdutta on 06/03/17.
 */

public abstract class TrackerFactoryUtil {

    public static TrackerFactory getTrackerFactory(TrackerFactoryType type) {

        switch (type) {
            case OFFLINE:
                return new OfflineTrackerFactory();
            case ONLINE:
                return new OnlineTrackerFactory();
            case HYBRID:
                return new HybridTrackerFactory();
            default:
                return null;
        }
    }

    public static TrackerFactory getDefaultTrackerFactory() {
        return getTrackerFactory(TrackerFactoryType.HYBRID);
    }

}
