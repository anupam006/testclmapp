package com.anupam.clmtracking.tracking.tracker.query.factory;

import com.anupam.clmtracking.tracking.tracker.query.api.TrackingDataWithCountQueryApi;

/**
 * Created by anupamdutta on 06/03/17.
 */

public interface TrackingDataWithCountQueryApiFactory {

    public TrackingDataWithCountQueryApi getTrackingDataWithCountQueryApi();

}
