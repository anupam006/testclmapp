package com.anupam.clmtracking.tracking.tracker.query.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.anupam.clmtracking.tracking.data.TrackingData;
import com.anupam.clmtracking.tracking.tracker.Tracker;

/**
 * Created by anupamdutta on 06/03/17.
 */

public class TrackingDataWithCount implements Parcelable {

    public String data;
    public int count;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.data);
        dest.writeInt(this.count);
    }

    public TrackingDataWithCount() {
    }

    protected TrackingDataWithCount(Parcel in) {
        this.data = in.readString();
        this.count = in.readInt();
    }

    public static final Parcelable.Creator<TrackingDataWithCount> CREATOR = new Parcelable.Creator<TrackingDataWithCount>() {
        @Override
        public TrackingDataWithCount createFromParcel(Parcel source) {
            return new TrackingDataWithCount(source);
        }

        @Override
        public TrackingDataWithCount[] newArray(int size) {
            return new TrackingDataWithCount[size];
        }
    };
}
