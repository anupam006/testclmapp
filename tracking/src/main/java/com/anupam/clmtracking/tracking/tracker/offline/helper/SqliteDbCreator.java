package com.anupam.clmtracking.tracking.tracker.offline.helper;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by anupamdutta on 06/03/17.
 */

public interface SqliteDbCreator {

    public void createDb(SQLiteDatabase db);

}
