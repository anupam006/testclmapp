package com.anupam.clmtracking.tracking.tracker.query.api;

import android.content.Context;

import com.anupam.clmtracking.tracking.tracker.query.data.TrackingDataWithCount;

import java.util.List;

/**
 * Created by anupamdutta on 06/03/17.
 */

public interface TrackingDataWithCountQueryApi {

    public List<TrackingDataWithCount> getTrackingDataWithCount(String trackingDataColumn, boolean desc);

    public List<TrackingDataWithCount> getTrackingDataWithCount(String trackingDataColumn, boolean desc, String whereClause);

    public void init(Context context);

}
