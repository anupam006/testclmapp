package com.anupam.clmtracking.tracking.tracker.offline.helper;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by anupamdutta on 06/03/17.
 */

public interface SqliteDbUpgrader {

    public void upgrade(SQLiteDatabase db, int oldVersion, int newVersion);

}
