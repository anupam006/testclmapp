package com.anupam.clmtracking.tracking.data;

/**
 * Created by anupamdutta on 06/03/17.
 */

public class TrackingData {

    /**
     * Store Tracker event values
     * For example. Name of Categories
     */
    public String trackingEventValue;

    /**
     * Store tracking event. For example "Category_Click" , "Category_Item_Click"
     */
    public String trackingEventType;


    public long currentTime;

}
