package com.anupam.clmtracking.tracking.tracker.offline.helper;

import android.database.sqlite.SQLiteDatabase;

import com.anupam.clmtracking.tracking.data.TrackingData;

/**
 * Created by anupamdutta on 06/03/17.
 */

public interface SqliteTrackingDataUpdater {

    public void update(SQLiteDatabase database, TrackingData data);

}
