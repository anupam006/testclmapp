package com.anupam.clmtracking.tracking.factory;

import com.anupam.clmtracking.tracking.factory.type.TrackerFactoryType;
import com.anupam.clmtracking.tracking.tracker.online.MixPanelTracker;
import com.anupam.clmtracking.tracking.tracker.Tracker;

/**
 * Created by anupamdutta on 06/03/17.
 */

public class OnlineTrackerFactory extends AbsTrackerFactory {

    public OnlineTrackerFactory() {
        super(TrackerFactoryType.ONLINE);
    }

    @Override
    public Tracker getTracker() {
        return new MixPanelTracker();
    }
}
