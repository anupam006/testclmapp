package com.anupam.clmtracking.tracking.factory;

import com.anupam.clmtracking.tracking.factory.type.TrackerFactoryType;
import com.anupam.clmtracking.tracking.tracker.hybrid.AllInOneTracker;
import com.anupam.clmtracking.tracking.tracker.online.MixPanelTracker;
import com.anupam.clmtracking.tracking.tracker.offline.SqliteOfflineTracker;
import com.anupam.clmtracking.tracking.tracker.Tracker;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anupamdutta on 06/03/17.
 */

public class HybridTrackerFactory extends AbsTrackerFactory {

    public HybridTrackerFactory() {
        super(TrackerFactoryType.HYBRID);
    }

    @Override
    public Tracker getTracker() {
        List<Tracker> trackerList = new ArrayList<>();
        trackerList.add(new SqliteOfflineTracker());
        trackerList.add(new MixPanelTracker());
        return new AllInOneTracker(trackerList);
    }
}
