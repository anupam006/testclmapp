package com.anupam.clmtracking.tracking.factory;

import com.anupam.clmtracking.tracking.factory.type.TrackerFactoryType;

/**
 * Created by anupamdutta on 06/03/17.
 */

public abstract class AbsTrackerFactory implements TrackerFactory {

    private TrackerFactoryType mTrackerFactoryType;

    public AbsTrackerFactory(TrackerFactoryType type) {
        mTrackerFactoryType = type;
    }

    @Override
    public TrackerFactoryType getTrackerFactoryType() {
        return mTrackerFactoryType;
    }

}
