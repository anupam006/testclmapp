package com.anupam.clmtracking.tracking.tracker.offline.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by anupamdutta on 06/03/17.
 */

public class SqliteOfflineTrackerDbHelper extends SQLiteOpenHelper {

    private SqliteDbCreator dbCreator;
    private SqliteDbUpgrader dbUpgrader;

    private static final String DB_NAME = "Tracking.db";
    private static final int DB_VERSION = 1;

    public SqliteOfflineTrackerDbHelper(Context context, SqliteDbCreator dbCreator, SqliteDbUpgrader dbUpgrader) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        if (dbCreator == null) {
            dbCreator = new SqliteOfflineDbCreator();
        }

        dbCreator.createDb(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if (dbUpgrader == null) {
            dbUpgrader = new SqliteOfflineDbUpgrader();
        }

        dbUpgrader.upgrade(db, oldVersion, newVersion);
        onCreate(db);
    }
}
