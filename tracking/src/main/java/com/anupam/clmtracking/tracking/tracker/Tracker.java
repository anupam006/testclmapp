package com.anupam.clmtracking.tracking.tracker;


import android.content.Context;

import com.anupam.clmtracking.tracking.data.TrackingData;
import com.anupam.clmtracking.tracking.tracker.type.TrackerType;

/**
 * Created by anupamdutta on 05/03/17.
 */

public interface Tracker {

    public void track(TrackingData trackingData);

    public TrackerType getTrackerType();

    public void init(Context context);

}
