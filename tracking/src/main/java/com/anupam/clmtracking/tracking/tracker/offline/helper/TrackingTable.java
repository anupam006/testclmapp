package com.anupam.clmtracking.tracking.tracker.offline.helper;

/**
 * Created by anupamdutta on 06/03/17.
 */

public class TrackingTable {

    public static final String TABLE_NAME = "Tracking";

    public static class Column {
        public static final String COLUMN_NAME_ID = "_id";
        public static final String COLUMN_NAME_EVENT_TYPE = "event_type";
        public static final String COLUMN_NAME_EVENT_VALUE = "event_value";
        public static final String COLUMN_NAME_EVENT_TIMESTAMP = "timestamp";
    }

}
