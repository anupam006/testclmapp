# README #

This README would contain details of the Application being developed.

### What is this repository for? ###

* Quick summary - This repository is being created for Testing Android Skills
* Version 1.0

### Purpose of this app ###

* Demonstrate the use of user based customization on frontend based on user behavior
* Demonstrate the use android library 


### Libraries Used ###
* GSON
* Picasso

### Libraries which could have been used ###

* Retrofit 2.0 for calling REST API
* Other community or team c